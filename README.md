# Void-dots 

Void Linux dot files. They are specially configured to work on my desktop environment, which is a hybrid between the Xfce desktop environment and the i3-gaps window manager. One may call it Xi3-gaps. 

![xfi4wmlogo](https://user-images.githubusercontent.com/64110504/93293153-8b2e8380-f7a4-11ea-885d-c97c8591f5af.png)

![Xi3](https://user-images.githubusercontent.com/64110504/93421491-ba112c00-f86e-11ea-81de-397bf6cd20a3.png)

